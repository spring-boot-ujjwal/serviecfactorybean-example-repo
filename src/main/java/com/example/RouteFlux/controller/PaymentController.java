package com.example.RouteFlux.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.RouteFlux.dto.PaymentRequest;
import com.example.RouteFlux.service.PaymentRegistry;
import com.example.RouteFlux.service.Paytm;
import com.example.RouteFlux.service.PhonePe;

@RestController
@RequestMapping("/payment-service")
public class PaymentController {
	
	@Autowired
	private PaymentRegistry paymentRegistry;
	
/*	@Autowired
	private Paytm paytm;
	
	@Autowired
	private PhonePe phonePe;
	
	@PostMapping("/pay")
	public String payNow(@RequestBody PaymentRequest request)
	{
		String response="";
		if(request.getPaymentMethod().equals("Paytm"))
			response = paytm.pay(request);
		
		if(request.getPaymentMethod().equals("PhonePe"))
			response = phonePe.pay(request);
		
		return response;
		
	}
*/
	
	@PostMapping("/pay")
	public String paymentProcess(@RequestBody PaymentRequest request)
	{
		String response="";
		
		response=paymentRegistry.getServiceBean(request.getPaymentMethod()).pay(request);
		  
		return response;
		
	}
	
}
