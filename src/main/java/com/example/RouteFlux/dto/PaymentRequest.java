package com.example.RouteFlux.dto;

public class PaymentRequest {

	private long amount;
	private String paymentMethod;
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public PaymentRequest(long amount, String paymentMethod) {
		super();
		this.amount = amount;
		this.paymentMethod = paymentMethod;
	}
	public PaymentRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
