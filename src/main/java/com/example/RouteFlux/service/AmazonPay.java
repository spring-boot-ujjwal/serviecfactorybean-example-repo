package com.example.RouteFlux.service;

import org.springframework.stereotype.Service;

import com.example.RouteFlux.dto.PaymentRequest;

@Service("AmazonPay")
public class AmazonPay implements PaymentService{

	@Override
	public String pay(PaymentRequest request) {
		return request.getAmount() + " paid succesfully using "+ request.getPaymentMethod();
	}

}
