package com.example.RouteFlux.service;


public interface PaymentRegistry {

	public PaymentService getServiceBean(String serviceName);
}
