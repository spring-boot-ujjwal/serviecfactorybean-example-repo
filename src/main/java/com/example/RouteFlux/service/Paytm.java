package com.example.RouteFlux.service;

import org.springframework.stereotype.Service;

import com.example.RouteFlux.dto.PaymentRequest;

@Service("Paytm")
public class Paytm implements PaymentService{

	@Override
	public String pay(PaymentRequest request) {
		return request.getAmount() + " paid succesfully using "+ request.getPaymentMethod();
	}

}
