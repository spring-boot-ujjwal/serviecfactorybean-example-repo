package com.example.RouteFlux.service;

import com.example.RouteFlux.dto.PaymentRequest;

public interface PaymentService {

    public String pay(PaymentRequest request);
}
