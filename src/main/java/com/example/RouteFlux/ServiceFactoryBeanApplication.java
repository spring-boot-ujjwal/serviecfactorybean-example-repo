package com.example.RouteFlux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceFactoryBeanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceFactoryBeanApplication.class, args);
	}

}
